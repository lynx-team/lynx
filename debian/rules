#!/usr/bin/make -f

export DEB_BUILD_MAINT_OPTIONS = hardening=+all
export DEB_CFLAGS_MAINT_APPEND := -Wall -DNO_BUILDSTAMP
BDIR = -O--builddirectory=build

SSL=gnu
# uncomment the next line if you want openssl version
#SSL=ssl

ifeq ($(SSL),gnu)
	CONFTLS := --with-gnutls=/usr
else
	CONFTLS := --with-ssl
endif

override_dh_auto_configure:
ifeq ($(SSL),gnu)
	sed -i -e s/libssl-dev/libgnutls28-dev/g debian/control
else
	sed -i -e s/libgnutls28-dev/libssl-dev/g debian/control
endif

	env cf_cv_SYSTEM_MAIL=/usr/sbin/sendmail \
	LIBS="-lbsd" \
	dh_auto_configure $(BDIR) --verbose -- \
		--enable-8bit-toupper \
		--enable-cgi-links \
		--enable-cjk \
		--enable-debug \
		--enable-default-colors \
		--enable-exec-links \
		--enable-exec-scripts \
		--enable-externs \
		--enable-forms-options \
		--enable-gzip-help \
		--enable-htmlized-cfg \
		--enable-ipv6 \
		--enable-japanese-utf8 \
		--enable-justify-elts \
		--enable-justify-elts \
		--enable-nested-tables \
		--enable-nls \
		--enable-nsl-fork \
		--enable-partial \
		--enable-persistent-cookies \
		--enable-prettysrc \
		--enable-read-eta \
		--enable-scrollbar \
		--enable-source-cache \
		--enable-syslog \
		--sysconfdir=/etc/lynx \
		--with-brotli \
		--with-bzlib \
		--without-included-gettext \
		--with-screen=ncursesw \
		--with-zlib \
		COMPRESS=/usr/bin/compress \
		ZIP=/usr/bin/zip \
		$(CONFTLS)

override_dh_autoreconf:
	dh_autoreconf $(BDIR) autoreconf-dickey -- -f -i

override_dh_auto_install:
	dh_auto_install $(BDIR) --verbose -- install-full
	cd debian/tmp/usr/share/lynx_help && rm -vf COPYING COPYHEADER
	cd debian/tmp/usr/share/lynx_doc && rm -rvf CHANGES \
		COPYHEADER.asc COPYING COPYING.asc samples test
	sed -i -e '/^# *\$$LynxId: /d' \
		-e '/^# *LYNX_VERSION  *"/d' \
		-e '/^# *LYNX_DATE  *"/d' \
		debian/tmp/etc/lynx/lynx.*

override_dh_link:
	rm -fv debian/lynx/usr/share/doc/lynx/changelog
	dh_link $(BDIR)

%:
	dh $@ --builddirectory=build
